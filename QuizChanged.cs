﻿using WindowsFormsApp1.DataObjects;

namespace WindowsFormsApp1
{
    public class QuizChangedSignature
    {
        public delegate void Signature(Quiz quiz);
    }
}
