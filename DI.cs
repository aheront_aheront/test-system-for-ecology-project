﻿using Autofac;
using WindowsFormsApp1.Models;
using WindowsFormsApp1.Presentators;
using WindowsFormsApp1.Views;

namespace WindowsFormsApp1
{
    internal class DI
    {
        private static IContainer _container;
        public static IContainer Container 
        { 
            get { 
                if(_container == null)
                    _container = BuildContainer();
                return _container;
            }
        }

        private static IContainer BuildContainer() { 
            var builder = new ContainerBuilder();
            builder.RegisterType<QuizView>().As<IQuizView>().InstancePerDependency();
            builder.RegisterType<QuizPresentator>().As<IQuizPresentator>().InstancePerDependency();
            builder.RegisterType<QuizModel>().As<IQuizModel>().InstancePerDependency();
            builder.RegisterType<ChooseTestView>().As<IChooseTestView>().InstancePerDependency();
            builder.RegisterType<ChooseTestPresentator>().As<IChooseTestPresentator>().InstancePerDependency();
            builder.RegisterType<ChooseTestModel>().As<IChooseTestModel>().InstancePerDependency();
            return builder.Build();
        }

    }
}
