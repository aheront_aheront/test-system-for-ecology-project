﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApp1.Presentators;
using WindowsFormsApp1.Views;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class ChooseTestView : Form, IChooseTestView
    {
        private IChooseTestPresentator _presentator;
        public ChooseTestView(IChooseTestPresentator chooseTestPresentator)
        {
            _presentator= chooseTestPresentator;
            InitializeComponent();
        }

        private void ChooseTestView_Shown(object sender, EventArgs e)
        {
            _presentator.AttachView(this);
        }

        public void SetTestChoice(List<string> tests)
        {
            TestList.Items.Clear();
            TestList.Items.AddRange(tests.ToArray());
            TestList.Items.Add("Выберите тест");
            TestList.SelectedIndex = tests.Count;
            this.ActiveControl = SendButton;
        }

        private void testList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(TestList.SelectedIndex == TestList.Items.Count-1)
            {
                SendButton.Enabled = false;
            } else SendButton.Enabled = true;
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            _presentator.TestChosen(TestList.SelectedIndex);
        }

        private void ChooseTestView_FormClosed(object sender, FormClosedEventArgs e)
        {
            _presentator.DetachView();
        }

        public void CloseView()
        {
            Close();
        }

        public void HideView()
        {
           Hide();
        }

        public void ShowView()
        {
            TestList.SelectedIndex = TestList.Items.Count-1;
            Show();
        }
    }
}
