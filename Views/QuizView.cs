﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApp1.DataObjects;
using WindowsFormsApp1.Presentators;
using WindowsFormsApp1.Views;

namespace WindowsFormsApp1
{
    public partial class QuizView : Form, IQuizView
    {
        
        private IQuizPresentator _presentator;
        List<CheckBox> _checkBoxes;
        Question _showingQuestion;
        public QuizView(IQuizPresentator quizPresentator)
        {
            InitializeComponent();
            _presentator = quizPresentator;
            _checkBoxes = new List<CheckBox> { Answer1, Answer2, Answer3, Answer4, Answer5 };
            foreach(var check in _checkBoxes)
            {
                check.CheckedChanged += new EventHandler(UpdateSendButtonEnabled);
            }
            _presentator.AttachView(this);
        }

        public event QuizChangedSignature.Signature QuizChanged;
        

        public bool AskToEndQuiz()
        {
            AskToEndQuizView form = new AskToEndQuizView();
            form.ShowDialog();
            return form.DialogResult == DialogResult.OK;
        }

        public void ChangeQuiz(Quiz quiz)
        {
            this.QuizChanged?.Invoke(quiz);
        }

        public List<string> GetUserAnswers()
        {
            List<string> answers = new List<string>();
            foreach(var checkBox in _checkBoxes)
            {
                if(checkBox.Checked)
                    answers.Add(checkBox.Text);
            }
            return answers;
        }

        public void SetNextEnabled(bool enabled)
        {
            GoNext.Enabled = enabled;
        }

        public void SetPreviousEnabled(bool enabled)
        {
            GoPrevious.Enabled = enabled;
        }

        public void ShowQuestion(Question question)
        {
            _showingQuestion = question;
            QuestionNumber.Text = $"{question.QuestionId}/{_presentator.GetQuestionCount()}";
            QuestionText.Text = question.QuestionText;
            QuestionImage.Image = question.Image;
            for(int checkBoxId = 0; checkBoxId < _checkBoxes.Count; checkBoxId++)
            {
                if (checkBoxId < question.AnswerVariants.Count)
                {
                    _checkBoxes[checkBoxId].Visible = true;
                    _checkBoxes[checkBoxId].Text = question.AnswerVariants[checkBoxId];
                    _checkBoxes[checkBoxId].Checked = question.UserAnswers.Contains(_checkBoxes[checkBoxId].Text);
                }
                else
                {
                    _checkBoxes[checkBoxId].Visible = false;
                    _checkBoxes[checkBoxId].Checked= false;
                }
            }
            UpdateSendButtonEnabled(this, null);
        }
        public void ShowView()
        {
            this.Show();
        }

        private void GoPrevious_Click(object sender, EventArgs e)
        {
            _presentator.PreviousPressed();
        }

        private void SendAnswer_Click(object sender, EventArgs e)
        {
            _presentator.SendAnswerPressed();
        }

        private void GoNext_Click(object sender, EventArgs e)
        {
            _presentator.NextPressed();
        }

        private void QuizView_FormClosed(object sender, FormClosedEventArgs e)
        {
            _presentator.DetachView();
        }

        private void UpdateSendButtonEnabled(object sender, EventArgs e)
        {
            SendAnswer.Enabled = _showingQuestion.UserAnswers.Count != 0;
            foreach(var check in _checkBoxes)
            {
                SendAnswer.Enabled |= check.Checked;
            } 
        }

        public void CloseView()
        {
            Close();
        }

        private void QuizView_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
