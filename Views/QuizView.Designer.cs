﻿namespace WindowsFormsApp1
{
    partial class QuizView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QuestionText = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Answer5 = new System.Windows.Forms.CheckBox();
            this.Answer4 = new System.Windows.Forms.CheckBox();
            this.Answer3 = new System.Windows.Forms.CheckBox();
            this.Answer2 = new System.Windows.Forms.CheckBox();
            this.Answer1 = new System.Windows.Forms.CheckBox();
            this.SendAnswer = new System.Windows.Forms.Button();
            this.GoPrevious = new System.Windows.Forms.Button();
            this.GoNext = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.QuestionNumber = new System.Windows.Forms.Label();
            this.QuestionImage = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QuestionImage)).BeginInit();
            this.SuspendLayout();
            // 
            // QuestionText
            // 
            this.QuestionText.AutoSize = true;
            this.QuestionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QuestionText.Location = new System.Drawing.Point(12, 53);
            this.QuestionText.MaximumSize = new System.Drawing.Size(600, 0);
            this.QuestionText.Name = "QuestionText";
            this.QuestionText.Size = new System.Drawing.Size(539, 87);
            this.QuestionText.TabIndex = 0;
            this.QuestionText.Text = "Выберите все правильные ответы. Что из перечисленного входит в список Всемирного " +
    "наследия ЮНЕСКО?";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Answer5);
            this.groupBox1.Controls.Add(this.Answer4);
            this.groupBox1.Controls.Add(this.Answer3);
            this.groupBox1.Controls.Add(this.Answer2);
            this.groupBox1.Controls.Add(this.Answer1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(17, 271);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(700, 240);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Варианты ответа";
            // 
            // Answer5
            // 
            this.Answer5.AutoSize = true;
            this.Answer5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Answer5.Location = new System.Drawing.Point(6, 200);
            this.Answer5.Name = "Answer5";
            this.Answer5.Size = new System.Drawing.Size(370, 30);
            this.Answer5.TabIndex = 4;
            this.Answer5.Text = "уникальные природные объекты";
            this.Answer5.UseVisualStyleBackColor = true;
            // 
            // Answer4
            // 
            this.Answer4.AutoSize = true;
            this.Answer4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Answer4.Location = new System.Drawing.Point(6, 160);
            this.Answer4.Name = "Answer4";
            this.Answer4.Size = new System.Drawing.Size(398, 30);
            this.Answer4.TabIndex = 3;
            this.Answer4.Text = "уникальные природные территории";
            this.Answer4.UseVisualStyleBackColor = true;
            // 
            // Answer3
            // 
            this.Answer3.AutoSize = true;
            this.Answer3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Answer3.Location = new System.Drawing.Point(6, 120);
            this.Answer3.Name = "Answer3";
            this.Answer3.Size = new System.Drawing.Size(302, 30);
            this.Answer3.TabIndex = 2;
            this.Answer3.Text = "уникальные издания книг ";
            this.Answer3.UseVisualStyleBackColor = true;
            // 
            // Answer2
            // 
            this.Answer2.AutoSize = true;
            this.Answer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Answer2.Location = new System.Drawing.Point(6, 80);
            this.Answer2.Name = "Answer2";
            this.Answer2.Size = new System.Drawing.Size(380, 30);
            this.Answer2.TabIndex = 1;
            this.Answer2.Text = "уникальные памятники культуры ";
            this.Answer2.UseVisualStyleBackColor = true;
            // 
            // Answer1
            // 
            this.Answer1.AutoSize = true;
            this.Answer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Answer1.Location = new System.Drawing.Point(6, 40);
            this.Answer1.Name = "Answer1";
            this.Answer1.Size = new System.Drawing.Size(367, 30);
            this.Answer1.TabIndex = 0;
            this.Answer1.Text = "уникальные природные явления";
            this.Answer1.UseVisualStyleBackColor = true;
            // 
            // SendAnswer
            // 
            this.SendAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SendAnswer.Location = new System.Drawing.Point(253, 517);
            this.SendAnswer.Name = "SendAnswer";
            this.SendAnswer.Size = new System.Drawing.Size(229, 56);
            this.SendAnswer.TabIndex = 2;
            this.SendAnswer.Text = "Ответить";
            this.SendAnswer.UseVisualStyleBackColor = true;
            this.SendAnswer.Click += new System.EventHandler(this.SendAnswer_Click);
            // 
            // GoPrevious
            // 
            this.GoPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GoPrevious.Location = new System.Drawing.Point(17, 517);
            this.GoPrevious.Name = "GoPrevious";
            this.GoPrevious.Size = new System.Drawing.Size(229, 56);
            this.GoPrevious.TabIndex = 3;
            this.GoPrevious.Text = "<- Назад";
            this.GoPrevious.UseVisualStyleBackColor = true;
            this.GoPrevious.Click += new System.EventHandler(this.GoPrevious_Click);
            // 
            // GoNext
            // 
            this.GoNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GoNext.Location = new System.Drawing.Point(488, 517);
            this.GoNext.Name = "GoNext";
            this.GoNext.Size = new System.Drawing.Size(229, 56);
            this.GoNext.TabIndex = 4;
            this.GoNext.Text = "Вперед ->";
            this.GoNext.UseVisualStyleBackColor = true;
            this.GoNext.Click += new System.EventHandler(this.GoNext_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Вопрос:";
            // 
            // QuestionNumber
            // 
            this.QuestionNumber.AutoSize = true;
            this.QuestionNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QuestionNumber.Location = new System.Drawing.Point(112, 13);
            this.QuestionNumber.Name = "QuestionNumber";
            this.QuestionNumber.Size = new System.Drawing.Size(72, 29);
            this.QuestionNumber.TabIndex = 6;
            this.QuestionNumber.Text = "1/100";
            // 
            // QuestionImage
            // 
            this.QuestionImage.Location = new System.Drawing.Point(667, 127);
            this.QuestionImage.Name = "QuestionImage";
            this.QuestionImage.Size = new System.Drawing.Size(10, 13);
            this.QuestionImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.QuestionImage.TabIndex = 7;
            this.QuestionImage.TabStop = false;
            this.QuestionImage.Visible = false;
            // 
            // QuizView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 589);
            this.Controls.Add(this.QuestionImage);
            this.Controls.Add(this.QuestionNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GoNext);
            this.Controls.Add(this.GoPrevious);
            this.Controls.Add(this.SendAnswer);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.QuestionText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuizView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QuizForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.QuizView_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.QuizView_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QuestionImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label QuestionText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox Answer5;
        private System.Windows.Forms.CheckBox Answer4;
        private System.Windows.Forms.CheckBox Answer3;
        private System.Windows.Forms.CheckBox Answer2;
        private System.Windows.Forms.CheckBox Answer1;
        private System.Windows.Forms.Button SendAnswer;
        private System.Windows.Forms.Button GoPrevious;
        private System.Windows.Forms.Button GoNext;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label QuestionNumber;
        private System.Windows.Forms.PictureBox QuestionImage;
    }
}