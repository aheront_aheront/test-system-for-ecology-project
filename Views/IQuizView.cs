﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsFormsApp1.DataObjects;

namespace WindowsFormsApp1.Views
{
    public interface IQuizView
    {
        event QuizChangedSignature.Signature QuizChanged;
        void ShowView();
        void ChangeQuiz(Quiz quiz);
        void ShowQuestion(Question question);
        List<string> GetUserAnswers();
        void SetNextEnabled(bool enabled);
        void SetPreviousEnabled(bool enabled);
        bool AskToEndQuiz();
        void CloseView();
    }
}
