﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1.Views
{
    public interface IChooseTestView
    {
        void SetTestChoice(List<string> tests);

        void HideView();
        void ShowView();
        void CloseView();
    }
}
