﻿namespace WindowsFormsApp1
{
    partial class ChooseTestView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TestList = new System.Windows.Forms.ComboBox();
            this.SendButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TestList
            // 
            this.TestList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TestList.FormattingEnabled = true;
            this.TestList.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.TestList.IntegralHeight = false;
            this.TestList.Location = new System.Drawing.Point(13, 13);
            this.TestList.Name = "TestList";
            this.TestList.Size = new System.Drawing.Size(206, 24);
            this.TestList.TabIndex = 0;
            this.TestList.SelectedIndexChanged += new System.EventHandler(this.testList_SelectedIndexChanged);
            // 
            // SendButton
            // 
            this.SendButton.Location = new System.Drawing.Point(226, 13);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(124, 24);
            this.SendButton.TabIndex = 1;
            this.SendButton.Text = "Выбрать";
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // ChooseTestView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 51);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.TestList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximumSize = new System.Drawing.Size(380, 98);
            this.MinimumSize = new System.Drawing.Size(380, 98);
            this.Name = "ChooseTestView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор теста";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ChooseTestView_FormClosed);
            this.Shown += new System.EventHandler(this.ChooseTestView_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox TestList;
        private System.Windows.Forms.Button SendButton;
    }
}

