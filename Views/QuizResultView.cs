﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApp1.DataObjects;

namespace WindowsFormsApp1.Views
{
    public partial class QuizResultView : Form
    {
        public QuizResultView()
        {
            InitializeComponent();
        }

        public bool ShowResult(QuizResult result)
        {
            Caption.Text = $"Тест завершен!";
            AchivedScore.Text = $"{result.UserScore}/{result.MaxScore}";
            Time.Text = $"{result.duration.Hours / 10}{result.duration.Hours % 10}:{result.duration.Minutes / 10}{result.duration.Minutes % 10}:{result.duration.Seconds / 10}{result.duration.Seconds % 10}";
            return this.ShowDialog() == DialogResult.OK || true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
