﻿using System;
using System.Collections.Generic;

namespace WindowsFormsApp1.DataObjects
{
    public class Quiz
    {
        public string Name;
        public List<Question> AllQuestions = new List<Question>();
        public List<Question> Questions = new List<Question>();
        public DateTime StartTime;
        public const int QuizQuestionCount = 20;
    }
}
