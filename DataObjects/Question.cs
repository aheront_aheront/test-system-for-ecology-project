﻿using System.Collections.Generic;
using System.Drawing;

namespace WindowsFormsApp1.DataObjects
{
    public class Question
    {
        public int QuestionId;
        public string QuestionText;
        public int Score;
        public Image Image;
        public List<string> AnswerVariants = new List<string>();
        public List<string> RightAnswers = new List<string>();
        public List<string> UserAnswers = new List<string>();
    }
}
