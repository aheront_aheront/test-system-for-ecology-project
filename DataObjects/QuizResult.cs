﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1.DataObjects
{
    public class QuizResult
    {
        public string QuizName;
        public int UserScore;
        public int MaxScore;
        public List<int> WrongAnsweredQuestionsList = new List<int>();
        public TimeSpan duration;

    }
}
