﻿using System.Collections.Generic;
using WindowsFormsApp1.DataObjects;

namespace WindowsFormsApp1.Models
{
    public interface IChooseTestModel
    {
        List<string> GetTestsList();

        Quiz CreateQuiz(int testIndex);
    }
}
