﻿using System;
using System.Collections.Generic;
using WindowsFormsApp1.DataObjects;

namespace WindowsFormsApp1.Models
{
    internal class QuizModel : IQuizModel
    {
        private Quiz _quiz = new Quiz();
        int currentQuestionId = 0;

        public Question GetCurrentQuestion() => _quiz.Questions[currentQuestionId];
     

        public int GetQuestionCount() => _quiz.Questions.Count;

        public void GoNextQuestion()
        {
            if (currentQuestionId + 1 >= GetQuestionCount())
                throw new InvalidOperationException();
            currentQuestionId++;
        }

        public void GoPreviousQuestion()
        {
            if(currentQuestionId - 1 < 0)
                throw new InvalidOperationException();
            currentQuestionId--;
        }
        public void GoNextUnansweredQuestion()
        {
            int unansweredQuestion = currentQuestionId;
            while (_quiz.Questions[unansweredQuestion].UserAnswers.Count != 0)
                unansweredQuestion++;
            currentQuestionId = unansweredQuestion;
        }

        public void GotoFirstUnansweredQuestion()
        {
            currentQuestionId = 0;
            GoNextUnansweredQuestion();
        }

        public void GotoQuestion(int questId)
        {
            if(questId < 0 || questId >= GetQuestionCount())
                throw new InvalidOperationException();
            currentQuestionId = questId;
        }

        public bool HaveNextQuestion() => currentQuestionId + 1 < GetQuestionCount();

        public bool HavePreviousQuestion() => currentQuestionId - 1 >= 0;

        public void UpdateQuiz(Quiz quiz)
        {
            _quiz = quiz;
            currentQuestionId = 0;
            _quiz.StartTime = DateTime.Now;
        }

        public void SaveUserAnswer(List<string> answers)
        {
            _quiz.Questions[currentQuestionId].UserAnswers = answers;
        }

        public bool HaveNextUnansweredQuiestion()
        {
            int unansweredQuestId = currentQuestionId;
            while (unansweredQuestId < GetQuestionCount())
            {
                if (_quiz.Questions[unansweredQuestId].UserAnswers.Count == 0)
                    return true;
                unansweredQuestId++;
            }
            return false;
        }

        public bool HaveUnansweredQuestions()
        {
            foreach(var quest in _quiz.Questions)
            {
                if (quest.UserAnswers.Count == 0)
                    return true;
            }
            return false;
        }

        private bool IsListsEqual(List<string> first, List<string> second)
        {
            if (first.Count != second.Count) return false;
            for (int i = 0; i < first.Count; i++)
                if (first[i].Equals(second[i]) == false)
                    return false;
            return true;
        }
        public QuizResult CreateResult()
        {
            QuizResult result = new QuizResult();
            result.QuizName = _quiz.Name;
            result.duration = DateTime.Now.Subtract(_quiz.StartTime);
            int maxScore = 0;
            int userScore = 0;
            foreach(var quest in _quiz.Questions)
            {
                quest.RightAnswers.Sort();
                quest.UserAnswers.Sort();
                if (IsListsEqual(quest.UserAnswers, quest.RightAnswers))
                {
                    userScore += quest.Score;
                }
                else
                {
                    result.WrongAnsweredQuestionsList.Add(quest.QuestionId);
                }
                maxScore += quest.Score;
            }
            result.UserScore = userScore;
            result.MaxScore = maxScore;
            return result;
        }
    }
}
