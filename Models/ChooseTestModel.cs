﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using WindowsFormsApp1.DataObjects;

namespace WindowsFormsApp1.Models
{
    internal class ChooseTestModel : IChooseTestModel
    {
        private List<string> tests = new List<string>();
        public ChooseTestModel()
        {
            try 
            {
                StreamReader reader = new StreamReader("Resources/config.cfg");
                while (!reader.EndOfStream)
                {
                    tests.Add(reader.ReadLine());
                }
                reader.Close();

            } catch(Exception ex) { }
        }

        public Quiz CreateQuiz(int testIndex)
        {
            Quiz quiz = new Quiz();
            quiz.Name = tests[testIndex].Replace(".cfg", "");
            StreamReader reader = new StreamReader($"Resources/{tests[testIndex]}");
            int questId = 1;
            Random rnd = new Random();
            while(!reader.EndOfStream) 
            {
                Question question = new Question();
                question.QuestionId = questId;
                question.QuestionText = reader.ReadLine();
                question.Score = int.Parse(reader.ReadLine().Trim());
                question.Image = Image.FromFile($"Resources/{reader.ReadLine()}");
                int answersCount = int.Parse(reader.ReadLine());
                int rightAnswerCount = int.Parse(reader.ReadLine());
                for(int i = 0; i < answersCount; i++)
                {
                    string answer = reader.ReadLine();
                    question.AnswerVariants.Add(answer);
                    if(i < rightAnswerCount)
                        question.RightAnswers.Add(answer);
                }

                Shuffle(ref question.AnswerVariants, rnd);
                quiz.AllQuestions.Add(question);
                questId++;
            }
            Shuffle(ref quiz.AllQuestions, rnd);
            for(int i = 0; i < quiz.AllQuestions.Count; i++)
            {
                quiz.AllQuestions[i].QuestionId = i+1;
            }
            List<Question> selectedQuestions = new List<Question>();
            for(int i = 0; i < Quiz.QuizQuestionCount; i++)
            {
                selectedQuestions.Add(quiz.AllQuestions[i]);
            }
            quiz.Questions = selectedQuestions;
            return quiz;
        }

        public List<string> GetTestsList()
        {
            List<string> croppedTests = new List<string>();
            tests.ForEach(test =>  croppedTests.Add(test.Replace(".cfg", "")));
            return croppedTests;
        }

        public static void Shuffle<T>(ref List<T> list, Random random)
        {
            int n = list.Count;

            for (int i = list.Count - 1; i > 1; i--)
            {
                int rnd = random.Next(i + 1);

                T value = list[rnd];
                list[rnd] = list[i];
                list[i] = value;
            }
        }
    }
}
