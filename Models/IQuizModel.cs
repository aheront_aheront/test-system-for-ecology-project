﻿using System.Collections.Generic;
using WindowsFormsApp1.DataObjects;

namespace WindowsFormsApp1.Models
{
    public interface IQuizModel
    {
        void UpdateQuiz(Quiz quiz);
        Question GetCurrentQuestion();
        void GoNextQuestion();
        void GoPreviousQuestion();
        int GetQuestionCount();
        bool HaveNextQuestion();
        bool HavePreviousQuestion();
        bool HaveUnansweredQuestions();
        void GotoQuestion(int questId);
        void GotoFirstUnansweredQuestion();
        void SaveUserAnswer(List<string> answers);
        void GoNextUnansweredQuestion();
        bool HaveNextUnansweredQuiestion();
        QuizResult CreateResult();
    }
}
