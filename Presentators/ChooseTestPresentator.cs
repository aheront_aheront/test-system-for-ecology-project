﻿using Autofac;
using System.Collections.Generic;
using WindowsFormsApp1.DataObjects;
using WindowsFormsApp1.Models;
using WindowsFormsApp1.Views;

namespace WindowsFormsApp1.Presentators
{
    internal class ChooseTestPresentator : IChooseTestPresentator
    {
        private IChooseTestView _view;
        private IChooseTestModel _model;

        public ChooseTestPresentator(IChooseTestModel model)
        {
            _model = model;
            QuizPresentator.QuizEnded += OnQuizEnd;
        }

        public void AttachView(IChooseTestView view)
        {
            _view = view;
            List<string> tests = _model.GetTestsList();
            if (tests.Count == 1) 
                TestChosen(0);
            _view.SetTestChoice(tests);
        }

        public void DetachView()
        {
            _view = null;
        }

        public void TestChosen(int TestIndex)
        {
            Quiz quiz = _model.CreateQuiz(TestIndex);
            IQuizView quizView = DI.Container.Resolve<IQuizView>();
            _view.HideView();
            quizView.ShowView();
            quizView.ChangeQuiz(quiz);
        }

        public void OnQuizEnd()
        {
            _view.ShowView();
        }

        ~ChooseTestPresentator()
        {
            QuizPresentator.QuizEnded -= OnQuizEnd;
        }
    }
}
