﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsFormsApp1.Views;

namespace WindowsFormsApp1.Presentators
{
    public interface IChooseTestPresentator
    {
        void AttachView(IChooseTestView view);
        void DetachView();
        void TestChosen(int index);
    }
}
