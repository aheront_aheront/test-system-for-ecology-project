﻿using WindowsFormsApp1.Views;

namespace WindowsFormsApp1.Presentators
{
    public interface IQuizPresentator
    {
        void AttachView(IQuizView view);
        void DetachView();
        void NextPressed();
        void PreviousPressed();
        void SendAnswerPressed();
        int GetQuestionCount();

    }
}
