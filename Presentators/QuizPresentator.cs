﻿using System.Collections.Generic;
using WindowsFormsApp1.DataObjects;
using WindowsFormsApp1.Models;
using WindowsFormsApp1.Views;

namespace WindowsFormsApp1.Presentators
{
    public class QuizPresentator : IQuizPresentator
    {
        private IQuizModel _model;
        private IQuizView _view;

        public delegate void QuizEnd();
        public static event QuizEnd QuizEnded;
        public QuizPresentator(IQuizModel model)
        {
            _model= model;
        }

        private void OnQuizChanged(Quiz quiz)
        {
            _model.UpdateQuiz(quiz);
            ShowCurrentQuestion();
     
        }

        public void AttachView(IQuizView view)
        {
            _view = view;
            _view.QuizChanged += OnQuizChanged;
        }

        public void DetachView()
        {
            _view.QuizChanged -= OnQuizChanged;
            _view = null;
            QuizEnded();
        }

        public void NextPressed()
        {
            if (_model.HaveNextQuestion() == false)
                return;
            _model.GoNextQuestion();
            ShowCurrentQuestion();
        }
        private void ShowCurrentQuestion()
        {
            _view.ShowQuestion(_model.GetCurrentQuestion());
            UpdateButtons();
        }
        private void UpdateButtons()
        {
            _view.SetNextEnabled(_model.HaveNextQuestion());
            _view.SetPreviousEnabled(_model.HavePreviousQuestion());
        }
        public void PreviousPressed()
        {
            if (_model.HavePreviousQuestion() == false)
                return;
            _model.GoPreviousQuestion();
            ShowCurrentQuestion();
        }

        public void SendAnswerPressed()
        {
            List<string> chosenAnswers = _view.GetUserAnswers();
            _model.SaveUserAnswer(chosenAnswers);
            if (_model.HaveNextUnansweredQuiestion())
            {
                _model.GoNextUnansweredQuestion();
                ShowCurrentQuestion();
                return;
            }
            TryEndQuiz();
        }

        private void TryEndQuiz()
        {
            if (_model.HaveUnansweredQuestions() == false)
            {
                EndQuiz();
                return;
            }
            bool wantToEndQuiz = _view.AskToEndQuiz();
            if (wantToEndQuiz) {
                EndQuiz();
                return;
            }

            _model.GotoFirstUnansweredQuestion();
            ShowCurrentQuestion();


        }

        public void EndQuiz()
        {
            var result = _model.CreateResult();
            QuizResultView resultView = new QuizResultView();
            bool CloseQuiz = resultView.ShowResult(result);
            if (CloseQuiz)
            {
                QuizEnded();
                _view.CloseView(); 
            }
        }

        public int GetQuestionCount()
        {
            return _model.GetQuestionCount();
        }
    }
}
