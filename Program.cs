﻿using Autofac;
using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using WindowsFormsApp1.Presentators;

namespace WindowsFormsApp1
{
    internal static class Program
    {
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.AssemblyResolve += LoadFromSubfolder;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form MainView = new ChooseTestView(DI.Container.Resolve<IChooseTestPresentator>());
            Application.Run(MainView);
        }

        
        static string binpath = "lib";
        static Assembly LoadFromSubfolder(object sender, ResolveEventArgs args)
        {
            string folderPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string assemblyPath = Path.Combine(folderPath + "", new AssemblyName(args.Name).Name + ".dll");
            if (!File.Exists(assemblyPath)) return null;
            Assembly assembly = Assembly.LoadFrom(assemblyPath);
            return assembly;
        }

    }
}
